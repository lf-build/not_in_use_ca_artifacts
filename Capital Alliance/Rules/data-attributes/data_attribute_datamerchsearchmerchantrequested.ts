function data_attribute_datamerchsearchmerchantrequestedrule(input:any) {
  var fein = '';
  var datamerchSearchMerchantResponse = input.Event.Data.Response.Merchants[0];
  fein = datamerchSearchMerchantResponse.Merchant.Fein;
  var result = {
    'Fein': fein
  };
  return {'datamerchReport':result};
}