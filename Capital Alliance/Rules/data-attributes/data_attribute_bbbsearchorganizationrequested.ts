function data_attribute_bbbsearchorganizationrequestedrule(input: any) {
    var ratingIcons = [];
    var businessURLs = '';
    var organizationType = '';
    var altOrganizationNames = '';
    var organizationLastChanged = '';
    var ratingLastChanged = '';
    var accreditationStatusLastChanged = '';
    var bbbsearchorganizationResponse = input.Event.Data.Response.SearchResults[0];
    organizationType = bbbsearchorganizationResponse.OrganizationType;
    ratingIcons = bbbsearchorganizationResponse.RatingIcons;
    businessURLs = bbbsearchorganizationResponse.BusinessURLs;
    altOrganizationNames = bbbsearchorganizationResponse.AltOrganizationNames;
    organizationLastChanged = bbbsearchorganizationResponse.OrganizationLastChanged;
    ratingLastChanged = bbbsearchorganizationResponse.RatingLastChanged;
    accreditationStatusLastChanged = bbbsearchorganizationResponse.AccreditationStatusLastChanged;
    var result = {
        'OrganizationType': organizationType,
        'RatingIcons': ratingIcons,
        'BusinessURLs': businessURLs,
        'AltOrganizationNames': altOrganizationNames,
        'OrganizationLastChanged': organizationLastChanged,
        'RatingLastChanged': ratingLastChanged,
        'AccreditationStatusLastChanged': accreditationStatusLastChanged
    };
    return {'bbbReport':result};
}
