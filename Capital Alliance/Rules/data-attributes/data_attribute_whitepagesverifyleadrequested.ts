function data_attribute_whitepagesverifyleadrequestedrule(input: any) {
    var phoneChecks = input.Event.Data.Response.PhoneChecks;
    var name = input.Event.Data.Response.Request.Name;
    var phone = input.Event.Data.Response.Request.Phone;
    var subscriberAddress = {
        'StreetLine1': phoneChecks.SubscriberAddress.StreetLine1,
        'StreetLine2': phoneChecks.SubscriberAddress.StreetLine2,
        'City': phoneChecks.SubscriberAddress.City,
        'PostalCode': phoneChecks.SubscriberAddress.PostalCode,
        'StateCode': phoneChecks.SubscriberAddress.StateCode,
        'CountryName': phoneChecks.SubscriberAddress.CountryName,
        'CountryCode': phoneChecks.SubscriberAddress.CountryCode
    };
    var result = {
        'Name': name,
        'Phone': phone,
        'IsConnected': phoneChecks.IsConnected,
        'SubscriberName': phoneChecks.SubscriberName,
        'PhoneToName': phoneChecks.PhoneToName,
        'IsPrepaid': phoneChecks.IsPrepaid,
        'IsCommercial': phoneChecks.IsCommercial,
        'SubscriberAddress': subscriberAddress,
        'error': null,
        'warnings': [],
        'isValid': true
    };
    return {'whitepageReport':result};
}