function data_attribute_yelpbusinessdetailrequestedrule(input:any) {
    var rating = '';   
    var isClosed = null;
    var yelpbusinessdetailResponse = input.Event.Data.Response.Businesses[0];
     rating = yelpbusinessdetailResponse.Rating;
     isClosed = yelpbusinessdetailResponse.IsClosed;    
    var result = {
        'Rating': rating,        
        'IsClosed': isClosed
    };
    return {'yelpReport':result};
}