function GeneratePlaidTransactionCashFlow(input: any) {
    var Plaidmonths = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
    ];
    var PlaidMonthlyCashFlowViewModel = {
        Name: '',
        Year: 0,
        BeginingBalance: 0,
        EndingBalance: 0,
        DepositCount: 0,
        WithdrawalCount: 0,
        TotalDepositAmount: 0,
        TotalWithdrawalAmount: 0,
        AverageDailyBalance: 0,
        FirstTransactionDate: '',
        MinDepositAmount: 0,
        MaxDepositAmount: 0,
        MinWithdrawalAmount: 0,
        MaxWithdrawalAmount: 0,
        NumberOfNSF: 0,
        NSFAmount: 0,
        LoanPaymentAmount: 0,
        NumberOfLoanPayment: 0,
        PayrollAmount: 0,
        NumberOfPayroll: 0,
        NumberOfNegativeBalance: 0,
        CustomAttributes: '',
    };
    var PlaidTransactionSummaryViewModel = {
        CountOfMonthlyStatement: 0,
        StartDate: '',
        EndDate: '',
        AverageDeposit: 0,
        AverageWithdrawal: 0,
        AverageDailyBalance: 0,
        NumberOfNegativeBalance: 0,
        NumberOfNSF: 0,
        NSFAmount: 0,
        LoanPaymentAmount: 0,
        NumberOfLoanPayment: 0,
        PayrollAmount: 0,
        NumberOfPayroll: 0,
        ChangeInDepositeVolume: 0
    };
    var PlaidCustomCashFlowViewModel = {
        ExcludedCategoriesDepositeCount: 0,
        ExcludedCategoriesMoreThanDepositeAttribute1: 0,
        ExcludedCategoriesMoreThanDepositeAttribute2: 0,
        IncludedCategoriesDebitAmount: 0,
        IncludedCategoriesDebitCount: 0
    };
    var PlaidCashFlowViewModel = {
        MonthlyCashFlows: [],
        TransactionSummary: {}
    };
    var AccountsCashFlow = {
        AccountNumber: '',
        CashFlow: PlaidCashFlowViewModel
    };
    var Category_Payroll = '21009000';
    var Category_LoanPayment = '16000000';
    var Category_NSF = '10007000';
    var Attribute1: number = 1500;
    var Attribute2: number = 10000;
    var MaxDepositePerMonthExcludeCategories = ['10001000', '10004000', '10008000', '15001000', '16002000', '16003000', '20000000', '20000001'];
    var LoanCategories = ['16003000', '18020004'];
    var cashFlowInput = input.Event.Data.Response;
    var endingBalance = 0;
    var beginingBalance = 0;
    var SelectedTransactions = [];
    var StartDate;
    var EndDate;
    var NegativeBalanceCount = 0;
    var allAccountsCashflows = [];
    var Accounts = [];
    Accounts = cashFlowInput.Accounts;
    Accounts.forEach(account => {
        var cashFlowResult = PlaidCashFlowViewModel;
        var objEndingBalance = cashFlowInput.Accounts.filter(function (item) {
            return item.Id == account.Id
        });
        if (objEndingBalance != null && objEndingBalance.length > 0) {
            endingBalance = objEndingBalance[0].Balance.Available;
            beginingBalance = endingBalance;
        }
        if (cashFlowInput != null && cashFlowInput.Transactions != null) {
            SelectedTransactions = cashFlowInput.Transactions.filter(function (input) {
                return input.Account == account.Id;
            });
        }
        if (SelectedTransactions != null && SelectedTransactions.length > 0) {
            var totalTransactions = SelectedTransactions.length;
            var MonthlyCashFlowsList = [];
            SelectedTransactions.sort((a, b) => {
                var start = +new Date(a.Date);
                var elapsed = +new Date(b.Date) - start;
                return elapsed;
            });
            EndDate = SelectedTransactions[0].Date;
            StartDate = SelectedTransactions[totalTransactions - 1].Date;
            var maxTransactionDate = new Date(EndDate);
            var currentMonth = maxTransactionDate.getMonth();
            var currentYear = maxTransactionDate.getFullYear();
            var isFirstTransaction = true;
            var isLastMonth = true;
            var dateOfLastTransaction;
            while (SelectedTransactions.length > 0) {
                var currentMonthTransactions = SelectedTransactions.filter(item => {
                    if (new Date(item.Date).getMonth() == currentMonth && new Date(item.Date).getFullYear() == currentYear) {
                        return item;
                    }
                });

                if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {

                    var currentMonthCashFlow = PlaidMonthlyCashFlowViewModel;
                    var objCustomAttributes = PlaidCustomCashFlowViewModel;
                    var monthlyDailyBalance = new Array<number>(31);
                    var MinDepositAmount = 0, MaxDepositAmount = 0, MinWithdrawalAmount = 0, MaxWithdrawalAmount = 0;
                    var DepositCount = 0, WithdrawalCount = 0, TotalMonthlyDepositAmount = 0, TotalMonthlyWithdrawalAmount = 0;
                    var TotalMonthlyLoanPaymentAmount = 0, TotalMonthlyLoanPaymentCount = 0;
                    var TotalMonthlyPayrollAmount = 0, TotalMonthlyPayrollCount = 0;
                    var TotalMonthlyNSFAmount = 0, TotalMonthlyNSFCount = 0;
                    var TotalMonthlyNegativeBalanceCount = 0;

                    currentMonthCashFlow.Name = Plaidmonths[currentMonth];
                    currentMonthCashFlow.Year = currentYear;
                    currentMonthCashFlow.EndingBalance = PlaidcustomRound(beginingBalance, 2);

                    currentMonthTransactions.forEach(objTransaction => {
                        if (objTransaction != undefined) {
                            if (beginingBalance < 0) {
                                NegativeBalanceCount++;
                                TotalMonthlyNegativeBalanceCount++;
                            }
                            if (objTransaction.CategoryId != undefined) {
                                if (objTransaction.Amount < 0 && MaxDepositePerMonthExcludeCategories.indexOf(objTransaction.CategoryId) == -1) {
                                    objCustomAttributes.ExcludedCategoriesDepositeCount++;
                                    var objTempDepositeAmount = Math.abs(objTransaction.Amount);
                                    if (objTempDepositeAmount >= Attribute1) {
                                        objCustomAttributes.ExcludedCategoriesDepositeCount++;
                                        objCustomAttributes.ExcludedCategoriesMoreThanDepositeAttribute1 = PlaidcustomRound(objCustomAttributes.ExcludedCategoriesMoreThanDepositeAttribute1 + objTempDepositeAmount, 2);
                                    }
                                    if (objTempDepositeAmount >= Attribute2) {
                                        objCustomAttributes.ExcludedCategoriesDepositeCount++;
                                        objCustomAttributes.ExcludedCategoriesMoreThanDepositeAttribute2 = PlaidcustomRound(objCustomAttributes.ExcludedCategoriesMoreThanDepositeAttribute2 + objTempDepositeAmount, 2);
                                    }
                                }
                                if (objTransaction.Amount > 0 && LoanCategories.indexOf(objTransaction.CategoryId) > -1) {
                                    objCustomAttributes.IncludedCategoriesDebitAmount = PlaidcustomRound(objCustomAttributes.IncludedCategoriesDebitAmount + objTransaction.Amount, 2);
                                    objCustomAttributes.IncludedCategoriesDebitCount++;
                                }

                                currentMonthCashFlow.CustomAttributes = JSON.stringify(objCustomAttributes);

                                if (objTransaction.CategoryId == Category_LoanPayment) {
                                    TotalMonthlyLoanPaymentAmount = TotalMonthlyLoanPaymentAmount + objTransaction.Amount;
                                    TotalMonthlyLoanPaymentCount = TotalMonthlyLoanPaymentCount + 1;
                                    currentMonthCashFlow.LoanPaymentAmount = TotalMonthlyLoanPaymentAmount;
                                    currentMonthCashFlow.NumberOfLoanPayment = TotalMonthlyLoanPaymentCount;
                                }
                                else if (objTransaction.CategoryId == Category_Payroll) {
                                    TotalMonthlyPayrollAmount = TotalMonthlyPayrollAmount + objTransaction.Amount;
                                    TotalMonthlyPayrollCount = TotalMonthlyPayrollCount + 1;
                                    currentMonthCashFlow.PayrollAmount = TotalMonthlyPayrollAmount;
                                    currentMonthCashFlow.NumberOfPayroll = TotalMonthlyPayrollCount;
                                }
                                else if (objTransaction.CategoryId == Category_NSF) {
                                    TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.Amount;
                                    TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
                                    currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
                                    currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
                                }
                            }

                            var currentTransactionDate: Date = new Date(objTransaction.Date);
                            var day: number = currentTransactionDate.getDate();

                            var calculateDailyEnding: boolean = isFirstTransaction ? isFirstTransaction :
                                (dateOfLastTransaction.getDate() != currentTransactionDate.getDate());

                            if (calculateDailyEnding) {
                                endingBalance = beginingBalance;
                                monthlyDailyBalance[day - 1] = endingBalance;
                            }
                            dateOfLastTransaction = currentTransactionDate;
                            var transactionAmount: number = objTransaction.Amount;

                            if (transactionAmount < 0) {
                                beginingBalance -= Math.abs(transactionAmount);

                                if (transactionAmount < MaxDepositAmount || MaxDepositAmount == 0) {
                                    MaxDepositAmount = transactionAmount;
                                }
                                if (transactionAmount > MinDepositAmount || MinDepositAmount == 0) {
                                    MinDepositAmount = transactionAmount;
                                }
                                DepositCount = DepositCount + 1;
                                TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
                            }
                            else {
                                beginingBalance += transactionAmount;
                                if (transactionAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
                                    MaxWithdrawalAmount = transactionAmount;
                                }
                                if (transactionAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
                                    MinWithdrawalAmount = transactionAmount;
                                }
                                WithdrawalCount = WithdrawalCount + 1;
                                TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + transactionAmount;
                            }

                            isFirstTransaction = false;
                            monthlyDailyBalance[day - 2] = beginingBalance;

                            var index = SelectedTransactions.indexOf(objTransaction, 0);
                            if (index > -1) {
                                SelectedTransactions.splice(index, 1);
                            }
                        }
                        totalTransactions -= 1;
                    });
                    currentMonthCashFlow.NumberOfNegativeBalance = PlaidcustomRound(TotalMonthlyNegativeBalanceCount, 2);
                    currentMonthCashFlow.TotalWithdrawalAmount = PlaidcustomRound(TotalMonthlyWithdrawalAmount, 2);
                    currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
                    currentMonthCashFlow.TotalDepositAmount = PlaidcustomRound(TotalMonthlyDepositAmount, 2);
                    currentMonthCashFlow.DepositCount = DepositCount;
                    currentMonthCashFlow.MinDepositAmount = MinDepositAmount;
                    currentMonthCashFlow.MaxDepositAmount = MaxDepositAmount;
                    currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
                    currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;


                    currentMonthCashFlow.FirstTransactionDate = PlaidcustomDate(dateOfLastTransaction);
                    currentMonthCashFlow.BeginingBalance = PlaidcustomRound(beginingBalance, 2);
                    var totalDaysInmonth = 0;
                    if (isLastMonth) {
                        totalDaysInmonth = maxTransactionDate.getDate();
                    }
                    else {
                        totalDaysInmonth = plaidGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
                    }

                    currentMonthCashFlow.AverageDailyBalance = PlaidcalculateAvgBalanceOfMonth(monthlyDailyBalance, totalDaysInmonth);
                    MonthlyCashFlowsList.push(PlaidsortObject(currentMonthCashFlow));
                    isLastMonth = false;
                }
                currentMonth -= 1;
                if (currentMonth < 0) {
                    currentYear -= 1;
                    currentMonth = 11;
                }
            }
            var objTransactionSummary = PlaidTransactionSummaryViewModel;
            objTransactionSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
            objTransactionSummary.StartDate = StartDate;
            objTransactionSummary.EndDate = EndDate;
            var TotalDepositAmount = 0;
            var TotalWithdrawalAmount = 0;
            var TotalDailyAverageBalance = 0;
            var TotalPayrollAmount = 0, TotalLoanPaymentAmount = 0, TotalNSFAmount = 0;
            var TotalPayrollCount = 0, TotalLoanPaymentCount = 0, TotalNSFCount = 0;
            MonthlyCashFlowsList.forEach(cashflow => {
                TotalDepositAmount += cashflow.TotalDepositAmount;
                TotalWithdrawalAmount += cashflow.TotalWithdrawalAmount;
                TotalDailyAverageBalance += cashflow.AverageDailyBalance;
                TotalPayrollAmount += cashflow.PayrollAmount;
                TotalLoanPaymentAmount += cashflow.LoanPaymentAmount;
                TotalNSFAmount += cashflow.NSFAmount;
                TotalPayrollCount += cashflow.NumberOfPayroll;
                TotalLoanPaymentCount += cashflow.NumberOfLoanPayment;
                TotalNSFCount += cashflow.NumberOfNSF;
            });
            objTransactionSummary.AverageDeposit = PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
            objTransactionSummary.AverageWithdrawal = PlaidcustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
            objTransactionSummary.AverageDailyBalance = PlaidcustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);

            objTransactionSummary.NumberOfNegativeBalance = NegativeBalanceCount;
            objTransactionSummary.NumberOfNSF = TotalNSFCount;
            objTransactionSummary.NSFAmount = PlaidcustomRound(TotalNSFAmount, 2);
            objTransactionSummary.LoanPaymentAmount = PlaidcustomRound((TotalLoanPaymentAmount), 2);
            objTransactionSummary.NumberOfLoanPayment = TotalLoanPaymentCount;
            objTransactionSummary.NumberOfPayroll = TotalPayrollCount;
            objTransactionSummary.PayrollAmount = PlaidcustomRound((TotalPayrollAmount), 2);
            cashFlowResult.MonthlyCashFlows = MonthlyCashFlowsList;
            var objTemp = PlaidsortObject(objTransactionSummary);
            cashFlowResult.TransactionSummary = objTemp;
        }
        var currentAccountCashflow = AccountsCashFlow;
        currentAccountCashflow.AccountNumber = account.Id;
        currentAccountCashflow.CashFlow = cashFlowResult;
        allAccountsCashflows.push(currentAccountCashflow);
    });
    return allAccountsCashflows;
}
function plaidGetDaysInMonth(month: any, year: any) {
    return new Date(year, month + 1, 0).getDate();
};
function PlaidcustomRound(value: any, precision: any) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
};
function PlaidsortObject(o: any) {
    var sorted = {},
        key, a = [];

    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }

    a.sort();

    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
};
function Plaidpad(s: any) { return (s < 10) ? '0' + s : s; };
function PlaidcustomDate(d: any) {
    return [Plaidpad(d.getMonth() + 1), Plaidpad(d.getDate()), d.getFullYear()].join('/');
};
function PlaidcalculateAvgBalanceOfMonth(dailyBalanceList: any, numberOfDaysInMonth: any) {
    var totalMonthlyDailyBalance = 0;
    var emptyCount = 1;
    var index = 0;
    while (index <= (numberOfDaysInMonth - 1)) {
        if (dailyBalanceList[index] == undefined) {
            emptyCount++;
        } else {
            totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
            emptyCount = 1;
        }
        index++;
    }
    return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
};
